source .env

echo "Delete existing containers..."
docker rm -f $APP

echo "gradle build app..."
gradle clean build

echo "Docker build image..."
aws ecr get-login-password --region ap-south-1 | docker login --username AWS --password-stdin 815113962699.dkr.ecr.ap-south-1.amazonaws.com
docker pull 815113962699.dkr.ecr.ap-south-1.amazonaws.com/tareqmy/mm2:latest

echo "Docker compose UP..."
docker-compose --file docker/docker-compose.yml up -d

docker logs -f $APP
