source .env

gradle clean build

aws ecr get-login-password --region ap-south-1 | docker login --username AWS --password-stdin 815113962699.dkr.ecr.ap-south-1.amazonaws.com
docker build -t mm2:latest .
docker tag mm2:latest 815113962699.dkr.ecr.ap-south-1.amazonaws.com/tareqmy/mm2:latest
docker push 815113962699.dkr.ecr.ap-south-1.amazonaws.com/tareqmy/mm2:latest
docker rmi 815113962699.dkr.ecr.ap-south-1.amazonaws.com/tareqmy/mm2:latest
