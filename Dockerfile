FROM openjdk:11-jre-slim
LABEL creator="Tareq Mohammad Yousuf"
LABEL email="tareq.y@gmail.com"

# creates the folder if doesnt exist
WORKDIR /mm2
COPY entrypoint.sh /opt/
RUN chmod +x /opt/*.sh

COPY build/libs/mm2-0.0.1-SNAPSHOT.jar /mm2/

EXPOSE 8080

CMD ["/opt/entrypoint.sh"]
