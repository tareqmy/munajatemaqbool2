package com.tareqmy.mm2.service.impl;

import com.tareqmy.mm2.service.DuaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import software.amazon.awssdk.services.dynamodb.model.GetItemRequest;
import software.amazon.awssdk.services.dynamodb.model.GetItemResponse;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tareqmy on 16/6/20.
 */
@Slf4j
@Service
public class DuaServiceImpl implements DuaService {

    @Value(value = "${mm2.aws.dynamodb.tableName}")
    private String tableName;

    private final DynamoDbClient dynamoDbClient;

    @Autowired
    public DuaServiceImpl(DynamoDbClient dynamoDbClient) {
        this.dynamoDbClient = dynamoDbClient;
    }

    @Override
    public GetItemResponse getItem(Long id) {
        Map<String, AttributeValue> keys = new HashMap<>();
        keys.put("type", AttributeValue.builder().s("dua").build());
        keys.put("id", AttributeValue.builder().n(String.valueOf(id)).build());
        return dynamoDbClient.getItem(GetItemRequest.builder()
            .tableName(tableName)
            .key(keys)
            .build());
    }
}
