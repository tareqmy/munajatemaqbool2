package com.tareqmy.mm2.service;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tareqmy on 18/6/20.
 */
@Getter
public class Day {

    private final String tags;

    private final String day;

    private final int number;

    private final int start;

    private final List<Integer> numbers;

    public Day(String tags, String day, int number, int start) {
        this.tags = tags;
        this.day = day;
        this.number = number;
        this.start = start;
        this.numbers = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            numbers.add(i+1);
        }
    }
}
