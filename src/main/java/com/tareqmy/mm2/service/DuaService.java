package com.tareqmy.mm2.service;

import software.amazon.awssdk.services.dynamodb.model.GetItemResponse;

/**
 * Created by tareqmy on 16/6/20.
 */
public interface DuaService {

    GetItemResponse getItem(Long id);
}
