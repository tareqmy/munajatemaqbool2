package com.tareqmy.mm2.controller;

import com.tareqmy.mm2.dto.DuaDTO;
import com.tareqmy.mm2.dto.MiscDTO;
import com.tareqmy.mm2.service.Day;
import com.tareqmy.mm2.service.DuaService;
import com.tareqmy.mm2.service.MiscService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Controller
public class IndexController {

    private final List<String> days = Arrays.asList("saturday", "sunday", "monday", "tuesday", "wednesday", "thursday", "friday");

    private final Map<String, Day> dayMap = new HashMap<>() {{
        put("saturday", new Day("saturday", "Saturday", 48, 1));
        put("sunday", new Day("sunday", "Sunday", 34, 49));
        put("monday", new Day("monday", "Monday", 31, 83));
        put("tuesday", new Day("tuesday", "Tuesday", 33, 114));
        put("wednesday", new Day("wednesday", "Wednesday", 22, 147));
        put("thursday", new Day("thursday", "Thursday", 15, 169));
        put("friday", new Day("friday", "Friday", 13, 184));
    }};

    private final DuaService duaService;

    private final MiscService miscService;

    @Autowired
    public IndexController(DuaService duaService, MiscService miscService) {
        this.duaService = duaService;
        this.miscService = miscService;
    }

    @GetMapping({"/"})
    public String getHome(Model model, @RequestParam(value = "id", required = false, defaultValue = "1") Long id) {
        DuaDTO duaDTO = new DuaDTO(duaService.getItem(id));
        model.addAttribute("dua", duaDTO);
        model.addAttribute("days", days); //unfortunately map keys are not sorted order
        model.addAttribute("dayMap", dayMap);
        model.addAttribute("numbersInDay", dayMap.get(duaDTO.getTags()).getNumbers());
        return "index";
    }

    @GetMapping({"/onday"})
    public String getDay(@RequestParam(value = "day", required = false, defaultValue = "saturday") String day,
                         @RequestParam(value = "number", required = false, defaultValue = "1") int number) {
        Day theDay = dayMap.get(day);
        if (theDay == null) {
            theDay = dayMap.get("saturday");
        }
        int id = theDay.getStart() + number - 1;
        return "redirect:/?id=" + id;
    }

    @GetMapping({"/introduction"})
    public String getIntro(Model model) {
        MiscDTO title = new MiscDTO(miscService.getItem(1L));
        MiscDTO basmalah = new MiscDTO(miscService.getItem(2L));
        MiscDTO content = new MiscDTO(miscService.getItem(3L));
        model.addAttribute("title", title);
        model.addAttribute("basmalah", basmalah);
        model.addAttribute("content", content);
        return "introduction";
    }

    @GetMapping({"/khutbah"})
    public String getKhutbah(Model model) {
        MiscDTO basmalah = new MiscDTO(miscService.getItem(5L));
        MiscDTO title = new MiscDTO(miscService.getItem(4L));
        MiscDTO content = new MiscDTO(miscService.getItem(6L));
        model.addAttribute("title", title);
        model.addAttribute("basmalah", basmalah);
        model.addAttribute("content", content);
        return "khutbah";
    }

    @GetMapping({"/about"})
    public String getAbout() {
        return "about";
    }
}
