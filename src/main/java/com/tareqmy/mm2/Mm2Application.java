package com.tareqmy.mm2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.tareqmy.mm2"})
public class Mm2Application {

    public static void main(String[] args) {
        SpringApplication.run(Mm2Application.class, args);
    }

}
