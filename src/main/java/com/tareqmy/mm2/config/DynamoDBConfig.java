package com.tareqmy.mm2.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.auth.credentials.ProfileCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;

/**
 * Created by tareqmy on 16/8/21.
 */
@Slf4j
@Configuration
public class DynamoDBConfig {

    @Value(value = "${mm2.aws.region}")
    private String region;

    @Bean
    public DynamoDbClient amazonAWSCredentials() {
        return DynamoDbClient.builder().
            region(Region.of(region))
//            .credentialsProvider(ProfileCredentialsProvider.builder()
//                .build())
            .build();
    }
}
