package com.tareqmy.mm2.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@Slf4j
@Configuration
@EnableScheduling
public class ScheduleConfig {

    @Value(value = "${spring.task.scheduling.pool.size}")
    public int poolSize;

    @Value(value = "${spring.task.scheduling.thread-name-prefix}")
    public String nameFormat;

    //note: this bean is needed when some scheduled action needs executed that cant be done through @Scheduled annotation
    //e.g scheduleAtFixedRate or scheduleWithFixedDelay
    @Bean(name = "threadPoolTaskScheduler")
    public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
        ThreadPoolTaskScheduler threadPoolTaskScheduler
            = new ThreadPoolTaskScheduler();
        threadPoolTaskScheduler.setPoolSize(poolSize);
        threadPoolTaskScheduler.setThreadNamePrefix(nameFormat);
        return threadPoolTaskScheduler;
    }
}
