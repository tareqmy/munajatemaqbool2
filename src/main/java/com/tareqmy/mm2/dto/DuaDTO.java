package com.tareqmy.mm2.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import software.amazon.awssdk.services.dynamodb.model.GetItemResponse;

/**
 * Created by tareqmy on 18/6/20.
 */
@NoArgsConstructor
@Getter
@Setter
@ToString
public class DuaDTO {

    private final Long startId = 1L;

    private final Long endId = 196L;

    private Long id;

    private int number;

    private String arabic;

    private String english;

    private String bengali;

    private String tags;

    private Long previous;

    private Long next;

    public DuaDTO(GetItemResponse item) {
        setId(Long.parseLong(item.item().get("id").n()));
        setNumber(Integer.parseInt(item.item().get("number").n()));
        setArabic(item.item().get("arabic").s());
        setEnglish(item.item().get("english").s());
        setBengali(item.item().get("bengali").s());
        setTags(item.item().get("tags").s());
        setPrevious(id - 1);
        setNext(id + 1);
        if (id.equals(startId)) {
            previous = endId;
        }
        if (id.equals(endId)) {
            next = startId;
        }
    }
}
